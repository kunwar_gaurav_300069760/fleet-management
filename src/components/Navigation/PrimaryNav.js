import React from 'react';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import Speed from "@material-ui/icons/Speed";
import DriveEta from "@material-ui/icons/DriveEta";
import Feedback from "@material-ui/icons/Feedback";
import AttachMoney from "@material-ui/icons/AttachMoney";
import Assessment from "@material-ui/icons/Assessment";
import NavItem from './NavItem';

const PrimaryNav = () => {
  return (
    <div>
      <NavItem route="/" name="Dashboard" Icon={Speed} />
      <NavItem route="/vehicles" name="Vehicles" Icon={DriveEta} />
      <NavItem route="/servicerequests" name="Service Requests" Icon={Feedback} />
      <NavItem route="/expenses" name="Expenses" Icon={AttachMoney} />
      <NavItem route="/drivers" name="Drivers" Icon={PermContactCalendarIcon} />
      <NavItem route="/reports" name="Reports" Icon={Assessment} />
    </div>
  )
};

export default PrimaryNav;
