export const reportingLine = {
    "status": {},
    "data": [
        {
            "driverDetails" : {
                "empId": 09874532,
                "status": "Active",
                "allotmentGroup": "Officers",
                "personalInfo" : {
                    "name": "Ashish P",
                    "phone": "9998887765",
                    "dob": "10 oct, 1988",
                    "address": "2nd cross, Bangalore"
                },
                "assignedVehicle": {
                    "name": "Toyota Innova",
                    "regNumber": "KA 07 G 3456",
                    "startDate": "17 Jan, 2020"
                }
            },
            "hierarchy" : [
                {
                    "name": "Bhaskar R",
                    "designation": "ADGP",
                    "location": "Karnataka",
                    "initials": "BR"
                },
                {
                    "name": "",
                    "designation": "CP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "DCP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "Officer",
                    "location": "",
                    "initials": ""
                },
                {
                    "name": "Ashish P",
                    "designation": "Driver",
                    "location": "Bangalore Urban District",
                    "initials": "AP"
                }
            ]
        },
        {
            "driverDetails" : {
                "empId": 09874535,
                "status": "Active",
                "allotmentGroup": "Cheetas",
                "personalInfo" : {
                    "name": "Venkat M",
                    "phone": "9998887765",
                    "dob": "10 Jul, 1989",
                    "address": "2nd cross, Bangalore"
                },
                "assignedVehicle": null
            },
            "hierarchy" : [
                {
                    "name": "Bhaskar R",
                    "designation": "ADGP",
                    "location": "Karnataka",
                    "initials": "BR"
                },
                {
                    "name": "",
                    "designation": "CP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "DCP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "Officer",
                    "location": "",
                    "initials": ""
                },
                {
                    "name": "Venkat M",
                    "designation": "Driver",
                    "location": "Bangalore Urban District",
                    "initials": "VM"
                }
            ]
        },
         {
            "driverDetails" : {
                "empId": 09874536,
                "status": "Active",
                "allotmentGroup": "Escorts",
                "personalInfo" : {
                    "name": "Rajan c",
                    "phone": "9998887766",
                    "dob": "10 Mar, 1987",
                    "address": "2nd cross, Bangalore"
                },
                 "assignedVehicle": {
                    "name": "Mahendra bolero",
                    "regNumber": "KA 53 G 6751",
                    "startDate": "17 Jun, 2020"
                }
            },
            "hierarchy" : [
                {
                    "name": "Bhaskar R",
                    "designation": "ADGP",
                    "location": "Karnataka",
                    "initials": "BR"
                },
                {
                    "name": "",
                    "designation": "CP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "DCP",
                    "location": "Bangalore City",
                    "initials": ""
                },
                {
                    "name": "",
                    "designation": "Officer",
                    "location": "",
                    "initials": ""
                },
                {
                    "name": "Rajan C",
                    "designation": "Driver",
                    "location": "Bangalore Urban District",
                    "initials": "RC"
                }
            ]
        }
    ]
}