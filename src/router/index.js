import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../components/Header";
import Navigation from "../components/Navigation";
import { Container } from "@material-ui/core";
import Dashboard from "../components/Dashboard/Dashboard";
import Vehicles from "../components/Vehicles/Vehicles";
import ServiceRequests from "../components/ServiceRequests/ServiceRequests";
import Expenses from "../components/Expenses/Expenses";
import DriversDetail from '../components/Drivers/DriversDetail/DriversDetail';
import Reports from "../components/Reports/Reports";

const useStyles = makeStyles((theme) => ({
	root: {
		display: "flex",
	},
	appBarSpacer: theme.mixins.toolbar,
	content: {
		height: "100vh",
		overflow: "auto",
	},
	container: {
		paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(4),
	},
}));

const App = () => {
	const [open, setOpen] = React.useState(true);

	const classes = useStyles();

	const handleDrawerToggle = () => {
		if (open) {
			setOpen(false);
			return;
		}
		setOpen(true);
	};

	return (
		<div className={classes.root}>
			<Header open={open} handleDrawerOpen={handleDrawerToggle} />
			{/* <CssBaseline /> */}
			<BrowserRouter>
				<Navigation open={open} handleDrawerClose={handleDrawerToggle} />
				<main className={classes.content}>
					<div className={classes.appBarSpacer} />
					<Container maxWidth="lg" className={classes.container}>
						<Switch>
							<Route exact path="/" component={Dashboard} />
							<Route path="/vehicles" component={Vehicles} />
							<Route path="/servicerequests" component={ServiceRequests} />
							<Route path="/expenses" component={Expenses} />

              <Route path="/drivers/:empId/reportingline" exact component={DriversDetail} />
              <Route path="/drivers/:empId" exact component={DriversDetail} />
              <Route path="/drivers" exact component={DriversDetail} />
							<Route path="/drivers" component={DriversDetail} />
							<Route path="/reports" component={Reports} />
						</Switch>
					</Container>
				</main>
			</BrowserRouter>
		</div>
	);
};

export default App;
